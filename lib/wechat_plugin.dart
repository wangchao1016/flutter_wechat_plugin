import 'dart:async';

import 'package:flutter/services.dart';

class WechatPlugin {
  static const MethodChannel _channel = const MethodChannel('wechat_plugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> register(String appId) async {
    var result = await _channel.invokeMethod('register', {'appId': appId});
    return result ?? 'result 为 空';
  }

  // static Future<dynamic> isWechatInstalled() async {
  //   var result = await _channel.invokeMethod('isWechatInstalled');
  //   return result == 'true' ? true : false;
  // }
}
