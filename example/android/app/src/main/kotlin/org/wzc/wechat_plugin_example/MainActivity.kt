package org.wzc.wechat_plugin_example

import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        
        // 相册 拍照 视频 第三方插件需要
        super.configureFlutterEngine(flutterEngine);
        // flutterEngine.getPlugins().add(FlutterBasicMessageChannel());
        GeneratedPluginRegistrant.registerWith(flutterEngine);


    }
}
