package org.wzc.wechat_plugin

import androidx.annotation.NonNull
import org.wzc.wechat_plugin.handlers.*
// import com.tencent.mm.opensdk.modelbiz.SubscribeMessage
// import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram
// import com.tencent.mm.opensdk.modelbiz.WXOpenBusinessWebview
// import com.tencent.mm.opensdk.modelpay.PayReq
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry



import android.util.Log;


// import com.tencent.mm.opensdk.modelbase.BaseReq;
// import com.tencent.mm.opensdk.modelbase.BaseResp;
// import com.tencent.mm.opensdk.modelmsg.SendAuth;
// import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
// import com.tencent.mm.opensdk.modelmsg.WXImageObject;
// import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
// import com.tencent.mm.opensdk.modelmsg.WXMusicObject;
// import com.tencent.mm.opensdk.modelmsg.WXTextObject;
// import com.tencent.mm.opensdk.modelmsg.WXVideoObject;
// import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
// import com.tencent.mm.opensdk.modelpay.PayReq;
// import com.tencent.mm.opensdk.openapi.IWXAPI;
// import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
// import com.tencent.mm.opensdk.openapi.WXAPIFactory;


// import java.io.BufferedInputStream;
// import java.io.BufferedOutputStream;
// import java.io.ByteArrayOutputStream;
// import java.io.IOException;
// import java.io.InputStream;
// import java.io.OutputStream;
// import java.net.HttpURLConnection;
// import java.net.URL;

/** WechatPlugin */
class WechatPlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity

  companion object {
      @JvmStatic
      fun registerWith(registrar: PluginRegistry.Registrar) {
        println("registerWith: 开始 ")
        val methodChannel = MethodChannel(registrar.messenger(), "wechat_plugin")
        // val authHandler = FluwxAuthHandler(channel)
        // FluwxResponseHandler.setMethodChannel(channel)
        println("registerWith: 已执行 ")
        WXApiHandler.setContext(registrar.activity().applicationContext)
        methodChannel.setMethodCallHandler(WechatPlugin())
        println("registerWith: 结束 ")
      }
  }

  //  private  var api: IWXAPI

  // private var applicationContext: Context? = null

  private lateinit var methodChannel : MethodChannel

  // private static final String APP_ID = "wx3edd9738b7cb3d7a";
  // 连接到引擎
  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    println("onAttachedToEngine: 开始 ")
    methodChannel = MethodChannel(flutterPluginBinding.binaryMessenger, "wechat_plugin")
    methodChannel.setMethodCallHandler(this)
    println("onAttachedToEngine: 结束 ")
    // onAttachedToEngine(flutterPluginBinding.binaryMessenger)
  }

  // private fun onAttachedToEngine(messenger: BinaryMessenger) {
  //     // this.applicationContext = applicationContext
  //     methodChannel = MethodChannel(messenger, "wechat_plugin")
  //     methodChannel?.setMethodCallHandler(this) //这个方法会直接调用onMethodCall接口，所以下面需要重写onMethodCall接口
  // }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "getPlatformVersion") {
      // println("call.method: ${call.method}")
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    }else if(call.method == "register") {
      // Log.v(call.method, call.argument("appId"))
      // var appId:String = call.argument("appId")
      // println("appId: $appId")
      println("WXApiHandler: 已执行 ")
      WXApiHandler.registerApp(call, result)
      // WXApiHandler.checkWeChatInstallation(result)
      // result.success(api.registerApp(appid));
      // result.success(call.argument("appId"));
    }else if(call.method == "isWechatInstalled"){
      // if()
      // result.error() // 返回失败
    } else {
      result.notImplemented()  //native 告诉 dart端，该方法没有实现
    }
  }

  // 脱离引擎
  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    methodChannel.setMethodCallHandler(null)
    // applicationContext = null
  }
}
